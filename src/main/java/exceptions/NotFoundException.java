package exceptions;

public class NotFoundException extends RuntimeException {

    public NotFoundException(int id){
        super("No se ha encontrado un objeto con id = " + id);
    }

}