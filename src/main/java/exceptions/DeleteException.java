package exceptions;

public class DeleteException extends RuntimeException {

    public DeleteException(int id){
        super("No se ha podido eliminar el objeto con id = " + id);
    }

}

