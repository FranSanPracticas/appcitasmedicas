package services.implementations;

import data.DTOs.DiagnosticoDTO;
import data.entities.Diagnostico;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DeadlockLoserDataAccessException;
import repositories.DiagnosticoRepository;
import services.IDiagnosticoService;

import java.util.LinkedList;
import java.util.List;

public class DiagnosticoService implements IDiagnosticoService {

    @Autowired
    DiagnosticoRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private DiagnosticoDTO mapToDTO(Diagnostico obj){
        DiagnosticoDTO dto = mapper.map(obj, DiagnosticoDTO.class);
        return dto;
    }

    private Diagnostico mapToEntity(DiagnosticoDTO dto){
        Diagnostico obj = mapper.map(dto, Diagnostico.class);
        return obj;
    }

    @Override
    public List<DiagnosticoDTO> getAll() {
        Iterable<Diagnostico> lista = repo.findAll();
        List<DiagnosticoDTO> listaDTO = new LinkedList<DiagnosticoDTO>();
        for (Diagnostico diag: lista) {
            listaDTO.add(mapToDTO(diag));
        }
        return listaDTO;
    }

    @Override
    public DiagnosticoDTO getById(int id) throws NotFoundException{
        if(!repo.existsById(id)){
            throw new NotFoundException(id);
        } else {
            return mapToDTO(repo.findById(id));
        }
    }

    @Override
    public DiagnosticoDTO add(DiagnosticoDTO obj) {
        repo.insert(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) throws DeleteException {
        if(repo.findById(id) == null){
            throw new DeleteException(id);
        }
        if(repo.findById(id) != null){
            repo.deleteById(id);
        }
    }

    @Override
    public void update(DiagnosticoDTO obj) {
        repo.update(mapToEntity(obj));
    }
}
