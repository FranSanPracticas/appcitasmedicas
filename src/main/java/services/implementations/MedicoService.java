package services.implementations;

import data.DTOs.MedicoDTO;
import data.entities.Medico;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import repositories.MedicoRepository;
import services.IMedicoService;

import java.util.LinkedList;
import java.util.List;

public class MedicoService implements IMedicoService {

    @Autowired
    MedicoRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private MedicoDTO mapToDTO(Medico obj){
        MedicoDTO dto = mapper.map(obj, MedicoDTO.class);
        return dto;
    }

    private Medico mapToEntity(MedicoDTO dto){
        Medico obj = mapper.map(dto, Medico.class);
        return obj;
    }


    @Override
    public List<MedicoDTO> getAll() {
        Iterable<Medico> lista = repo.findAll();
        List<MedicoDTO> listaDTO = new LinkedList<MedicoDTO>();
        for (Medico diag: lista) {
            listaDTO.add(mapToDTO(diag));
        }
        return listaDTO;
    }

    @Override
    public MedicoDTO getById(int id) throws NotFoundException {
        if(!repo.existsById(id)){
            throw new NotFoundException(id);
        } else {
            return mapToDTO(repo.findById(id));
        }
    }

    @Override
    public MedicoDTO add(MedicoDTO obj) {

        repo.insert(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) throws DeleteException {
        if(repo.findById(id) == null){
            throw new DeleteException(id);
        }
        if(repo.findById(id) != null){
            repo.deleteById(id);
        }
    }

    @Override
    public void update(MedicoDTO obj) {
        repo.update(mapToEntity(obj));
    }
}
