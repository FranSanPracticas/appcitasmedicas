package services.implementations;

import data.DTOs.CitaDTO;
import data.entities.Cita;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import repositories.CitaRepository;
import services.ICitaService;

import java.util.LinkedList;
import java.util.List;

public class CitaService implements ICitaService {

    @Autowired
    CitaRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private CitaDTO mapToDTO(Cita obj){
        CitaDTO dto = mapper.map(obj, CitaDTO.class);
        return dto;
    }

    private Cita mapToEntity(CitaDTO dto){
        Cita obj = mapper.map(dto, Cita.class);
        return obj;
    }




    @Override
    public List<CitaDTO> getAll() {
        Iterable<Cita> lista = repo.findAll();
        List<CitaDTO> listaDTO = new LinkedList<CitaDTO>();
        for (Cita diag: lista) {
            listaDTO.add(mapToDTO(diag));
        }
        return listaDTO;
    }

    @Override
    public CitaDTO getById(int id) throws NotFoundException {
        if(!repo.existsById(id)){
            throw new NotFoundException(id);
        } else {
            return mapToDTO(repo.findById(id));
        }
    }

    @Override
    public CitaDTO add(CitaDTO obj) {

        repo.insert(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) throws DeleteException {
        if(repo.findById(id) == null){
            throw new DeleteException(id);
        }
        if(repo.findById(id) != null){
            repo.deleteById(id);
        }
    }

    @Override
    public void update(CitaDTO obj) {
        repo.update(mapToEntity(obj));
    }
}
