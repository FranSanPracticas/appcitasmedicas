package services.implementations;

import data.DTOs.PacienteDTO;
import data.entities.Paciente;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.PacienteRepository;
import services.IPacienteService;

import java.util.LinkedList;
import java.util.List;

@Service
public class PacienteService implements IPacienteService {


    @Autowired
    PacienteRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private PacienteDTO mapToDTO(Paciente obj){
        PacienteDTO dto = mapper.map(obj, PacienteDTO.class);
        return dto;
    }

    private Paciente mapToEntity(PacienteDTO dto){
        Paciente obj = mapper.map(dto, Paciente.class);
        return obj;
    }


    @Override
    public List<PacienteDTO> getAll() {
        Iterable<Paciente> lista = repo.findAll();
        List<PacienteDTO> listaDTO = new LinkedList<PacienteDTO>();
        for (Paciente diag: lista) {
            listaDTO.add(mapToDTO(diag));
        }
        return listaDTO;
    }

    @Override
    public PacienteDTO getById(int id) throws NotFoundException {
        if(!repo.existsById(id)){
            throw new NotFoundException(id);
        } else {
            return mapToDTO(repo.findById(id));
        }
    }

    @Override
    public PacienteDTO add(PacienteDTO obj) {

        repo.insert(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) throws DeleteException {
        if(repo.findById(id) == null){
            throw new DeleteException(id);
        }
        if(repo.findById(id) != null){
            repo.deleteById(id);
        }
    }

    @Override
    public void update(PacienteDTO obj) {
        repo.update(mapToEntity(obj));
    }
}
