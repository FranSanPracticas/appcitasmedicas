package services;

import data.DTOs.PacienteDTO;

import java.util.List;

public interface IPacienteService {

    public List<PacienteDTO> getAll();
    public PacienteDTO getById(int id);
    public PacienteDTO add(PacienteDTO obj);
    public void delete(int id);
    public void update(PacienteDTO obj);
}
