package services;

import data.DTOs.CitaDTO;

import java.util.List;

public interface ICitaService {

    public List<CitaDTO> getAll();
    public CitaDTO getById(int id);
    public CitaDTO add(CitaDTO obj);
    public void delete(int id);
    public void update(CitaDTO obj);
}
