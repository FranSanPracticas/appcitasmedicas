package repositories;

import data.entities.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findById(int id);
    List<Usuario> getAll();
    void deleteById(int id);
    void insert(Usuario obj);
    void update(Usuario obj);
    void save();

}
