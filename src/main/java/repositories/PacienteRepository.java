package repositories;

import data.entities.Paciente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PacienteRepository extends CrudRepository<Paciente, Integer>{

    Paciente findById(int id);
    List<Paciente> getAll();
    void deleteById(int id);
    void insert(Paciente obj);
    void update(Paciente obj);
    void save();
}
