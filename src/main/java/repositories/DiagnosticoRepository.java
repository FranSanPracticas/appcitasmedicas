package repositories;

import data.entities.Diagnostico;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DiagnosticoRepository extends CrudRepository<Diagnostico, Integer> {

    Diagnostico findById(int id);
    List<Diagnostico> getAll();
    void deleteById(int id);
    void insert(Diagnostico obj);
    void update(Diagnostico obj);
    void save();


}
