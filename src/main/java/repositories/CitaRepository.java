package repositories;

import data.entities.Cita;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CitaRepository extends CrudRepository<Cita, Integer>{

    Cita findById(int id);
    List<Cita> getAll();
    void deleteById(int id);
    void insert(Cita obj);
    void update(Cita obj);
    void save();
}
