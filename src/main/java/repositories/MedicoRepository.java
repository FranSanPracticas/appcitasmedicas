package repositories;

import data.entities.Medico;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MedicoRepository extends CrudRepository<Medico, Integer>{

    Medico findById(int id);
    List<Medico> getAll();
    void deleteById(int id);
    void insert(Medico obj);
    void update(Medico obj);
    void save();

}
