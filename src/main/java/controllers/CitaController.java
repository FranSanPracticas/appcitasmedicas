package controllers;

import data.DTOs.CitaDTO;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.implementations.CitaService;

import java.util.List;

@RestController
public class CitaController {

    private static final System.Logger logger = System.getLogger("c.f.b.DefaultLogger");

    @Autowired
    CitaService service;

    @GetMapping("/citas")
    public List<CitaDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/citas/{id}")
    public CitaDTO getById(@PathVariable int id){
        try{
            CitaDTO dto = service.getById(id);
            return dto;
        } catch (NotFoundException e){
            logger.log(System.Logger.Level.ERROR, "NotFoundException --> No se ha encontrado un objeto con id " + id);
        }
        return null;
    }

    @PostMapping("/citas/add")
    public CitaDTO add(@RequestBody CitaDTO citaDTO){
        return service.add(citaDTO);
    }

    @DeleteMapping("/citas/delete/{id}")
    public void delete(@PathVariable int id){
        try{
            service.delete(id);
        } catch (DeleteException e){
            logger.log(System.Logger.Level.ERROR, "DeleteException --> No se ha podido eliminar el objeto con Id " + id);
        }
    }

    @PutMapping("/citas/update")
    public void update(@RequestBody CitaDTO citaDTO){
        service.update(citaDTO);
    }
}
