package controllers;

import data.DTOs.MedicoDTO;
import data.DTOs.PacienteDTO;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.implementations.MedicoService;
import services.implementations.PacienteService;

import java.util.List;

@RestController
public class PacienteController {

    private static final System.Logger logger = System.getLogger("c.f.b.DefaultLogger");

    @Autowired
    PacienteService service;

    @GetMapping("/pacientes")
    public List<PacienteDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/pacientes/{id}")
    public PacienteDTO getById(@PathVariable int id){
        try{
            PacienteDTO dto = service.getById(id);
            return dto;
        } catch (NotFoundException e){
            logger.log(System.Logger.Level.ERROR, "NotFoundException --> No se ha encontrado un objeto con id " + id);
        }
        return null;
    }

    @PostMapping("/pacientes/add")
    public PacienteDTO add(@RequestBody PacienteDTO pacienteDTO){
        return service.add(pacienteDTO);
    }

    @DeleteMapping("/pacientes/delete/{id}")
    public void delete(@PathVariable int id){
        try{
            service.delete(id);
        } catch (DeleteException e){
            logger.log(System.Logger.Level.ERROR, "DeleteException --> No se ha podido eliminar el objeto con Id " + id);
        }
    }

    @PutMapping("/pacientes/update")
    public void update(@RequestBody PacienteDTO pacienteDTO){
        service.update(pacienteDTO);
    }
}
