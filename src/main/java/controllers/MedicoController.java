package controllers;

import data.DTOs.MedicoDTO;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.implementations.MedicoService;

import java.util.List;

@RestController
public class MedicoController {

    private static final System.Logger logger = System.getLogger("c.f.b.DefaultLogger");

    @Autowired
    MedicoService service;

    @GetMapping("/medicos")
    public List<MedicoDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/medicos/{id}")
    public MedicoDTO getById(@PathVariable int id){
        try{
            MedicoDTO dto = service.getById(id);
            return dto;
        } catch (NotFoundException e){
            logger.log(System.Logger.Level.ERROR, "NotFoundException --> No se ha encontrado un objeto con id " + id);
        }
        return null;
    }

    @PostMapping("/medicos/add")
    public MedicoDTO add(@RequestBody MedicoDTO medicoDTO){
        return service.add(medicoDTO);
    }

    @DeleteMapping("/medicos/delete/{id}")
    public void delete(@PathVariable int id){
        try{
            service.delete(id);
        } catch (DeleteException e){
            logger.log(System.Logger.Level.ERROR, "DeleteException --> No se ha podido eliminar el objeto con Id " + id);
        }
    }

    @PutMapping("/medicos/update")
    public void update(@RequestBody MedicoDTO medicoDTO){
        service.update(medicoDTO);
    }
}
