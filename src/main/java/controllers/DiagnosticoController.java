package controllers;

import data.DTOs.DiagnosticoDTO;
import exceptions.DeleteException;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import services.implementations.DiagnosticoService;

import java.util.List;


@RestController
public class DiagnosticoController {

    private static final System.Logger logger = System.getLogger("c.f.b.DefaultLogger");

    @Autowired
    DiagnosticoService service;

    @GetMapping("/diagnosticos")
    public List<DiagnosticoDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/diagnosticos/{id}")
    public DiagnosticoDTO getById(@PathVariable int id){
        try{
            DiagnosticoDTO dto = service.getById(id);
            return dto;
        } catch (NotFoundException e){
            logger.log(System.Logger.Level.ERROR, "NotFoundException --> No se ha encontrado un objeto con id " + id);
        }
        return null;
    }

    @PostMapping("/diagnosticos/add")
    public DiagnosticoDTO add(@RequestBody DiagnosticoDTO diagnosticoDTO){
        return service.add(diagnosticoDTO);
    }

    @DeleteMapping("/diagnosticos/delete/{id}")
    public void delete(@PathVariable int id){
        try{
            service.delete(id);
        } catch (DeleteException e){
            logger.log(System.Logger.Level.ERROR, "DeleteException --> No se ha podido eliminar el objeto con Id " + id);
        }
    }

    @PutMapping("/diagnosticos/update")
    public void update(@RequestBody DiagnosticoDTO diagnosticoDTO){
        service.update(diagnosticoDTO);
    }
}
