package data.DTOs.Child;

import data.DTOs.UsuarioDTO;

import javax.persistence.Column;

public class PacienteChildDTO extends UsuarioDTO {

    private String NSS;
    private String numTarjeta;
    private String telefono;
    private String direccion;

    public String getNSS() {
        return NSS;
    }
    public void setNSS(String NSS) {
        this.NSS = NSS;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }
    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
