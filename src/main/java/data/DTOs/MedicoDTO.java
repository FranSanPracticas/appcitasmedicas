package data.DTOs;

import data.DTOs.Child.PacienteChildDTO;

import java.util.LinkedList;
import java.util.List;

public class MedicoDTO {

    private String numColegiado;

    private List<CitaDTO> citas;
    private List<PacienteChildDTO> pacientes;

    public String getNumColegiado() {
        return numColegiado;
    }
    public void setNumColegiado(String numColegiado) {
        this.numColegiado = numColegiado;
    }

    public List<CitaDTO> getCitas() {
        return new LinkedList<CitaDTO>(citas);
    }
    public void setCitas(List<CitaDTO> citas) {
        this.citas = citas;
    }

    public List<PacienteChildDTO> getPacientes() {
        return new LinkedList<PacienteChildDTO>(pacientes);
    }
    public void setPaciente(List<PacienteChildDTO> pacientes) {
        this.pacientes = pacientes;
    }


}
