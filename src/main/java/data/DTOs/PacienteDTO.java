package data.DTOs;

import data.DTOs.Child.MedicoChildDTO;

import java.util.LinkedList;
import java.util.List;

public class PacienteDTO {

    private String NSS;
    private String numTarjeta;
    private String telefono;
    private String direccion;

    private List<CitaDTO> citas;
    private List<MedicoChildDTO> medicos;


    public String getNSS() {
        return NSS;
    }
    public void setNSS(String NSS) {
        this.NSS = NSS;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }
    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    public List<CitaDTO> getCitas() {
        return new LinkedList<CitaDTO>(citas);
    }
    public void setCitas(List<CitaDTO> citas) {
        this.citas = citas;
    }

    public List<MedicoChildDTO> getMedicos() {
        return new LinkedList<MedicoChildDTO>(medicos);
    }
    public void setMedicos(List<MedicoChildDTO> medicos) {
        this.medicos = medicos;
    }



}
